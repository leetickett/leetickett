# My Journey

2007: Started my own company, Tickett Enterprises Limited (comprised of just me).

2012: Employed my first member of staff to help with the workload.

Over the following years my team grew (at most to around 15 employees), but experienced
scaling challenges (partly down to not having the right processes/tools in place).
We're now at a more manageable 6, and hopefully more equipped to grow!

2016: Moved from my home office into an office based in Southend-on-Sea.

2018: Migrated from Subversion (SVN) to Git. Frustratingly, I don't remember why
I chose GitLab (but I'm glad I did).

Over the following year, my team and I built some GitLab extensions using Webhooks
and the API. Before coming to the realization that we could contribute to the project,
making our feature available to everyone.

2019: Joined a few [GitLab hackathons](https://about.gitlab.com/community/hackathon/),
which got me excited about contributing.

2020: Joined the [GitLab core team](https://about.gitlab.com/community/core-team/).

2020: Shared [my first GitLab unfiltered blog post](https://about.gitlab.com/blog/2020/11/13/lee-tickett-my-gitlab-journey/).

2021: Awarded [GitLab 13.12 MVP](https://about.gitlab.com/community/mvp/).

2022: Shared a follow up [GitLab blog post](https://about.gitlab.com/blog/2022/05/12/gitlab-heroes-unmasked-elevating-my-company-using-gitlab/).

2022: Launched the GitLab CRM [1](https://about.gitlab.com/releases/2022/05/22/gitlab-15-0-released/)
[2](https://docs.gitlab.com/ee/user/crm/).

2022: @espadav8, @zillemarco and I started to formalize our [community pairing sessions](https://gitlab.com/community-coders/pairing-sessions).
